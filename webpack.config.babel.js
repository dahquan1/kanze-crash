import webpack from 'webpack';
import path, { resolve } from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const DEV = (process.env.NODE_ENV === 'development');

const core = {
    devtool: DEV ? 'cheap-module-source-map' : false,
    entry: [
        ...(DEV ? [
            'react-hot-loader/patch',
            'webpack-dev-server/client?http://localhost:8080',
            'webpack/hot/only-dev-server',
            path.join(__dirname, 'lib', 'app.js'),
        ] : [
            path.join(__dirname, 'lib', 'components', 'component.js'),
        ]),
    ],
    output: {
        path: DEV ? path.join(__dirname, 'build') : path.join(__dirname, 'dist'),
        filename: DEV ? 'app.js' : 'component.js',
        ...(DEV ? {} : {
            library: 'base-component',
            libraryTarget: 'umd',
            umdNamedDefine: true
        })
    },
    devServer: {
        hot: true,
        contentBase: resolve(__dirname, 'build'),
        publicPath: '/',
    },
    module: {
        loaders: [
            /* {
                test: /\.js$/,
                loader: "eslint-loader",
                enforce: "pre",
                exclude: /(node_modules|server.js)/
            },*/
            {
                exclude: [
                    /\.html$/,
                    /\.(js|jsx)$/,
                    /\.css$/,
                    /\.json$/,
                    /\.svg$/,
                ],
                loader: 'url-loader',
                query: {
                    limit: 10000,
                    name: 'static/media/[name].[hash:8].[ext]',
                },
            },
            {
                test: /\.(js|jsx)$/,
                include: path.join(__dirname, '/lib'),
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                },
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
            },
            {
                test: /\.svg$/,
                loader: 'file-loader',
                query: {
                    name: 'static/media/[name].[hash:8].[ext]',
                },
            },
            {
              test: /\.css$/,
              include: /lib/,
              loaders: [
                'style-loader',
                'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'// ,
                // 'postcss-loader'
              ]
            }
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.css', '.styl', '.png', '.jpg', '.jpeg'],
    },
    externals: (DEV ? {} : {
        react: 'react'
    }),
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: process.env.NODE_ENV,
        }),

        ...(DEV ? [
            new webpack.HotModuleReplacementPlugin(),
            new HtmlWebpackPlugin({
                inject: true,
                template: path.join(__dirname, 'lib', 'public', 'index.html'),
            }),
        ] : [
            new webpack.optimize.AggressiveMergingPlugin(),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                },
            }),
        ]),
    ],
};


module.exports = core;


import React from 'react'
import ee from 'event-emitter'

import {
  CrashGraph,
  STATE_NOT_STARTED,
  STATE_IN_PROGRESS,
  STATE_STARTING,
  STATE_OVER
} from './components/CrashGraph'
import style from './demo.css'

class FakeGame {
  constructor() {
    this.state = STATE_NOT_STARTED
  }

  start() {
    // Start the game in 7 seconds
    this.state = STATE_STARTING
    this.startTime = Date.now() + 7000
    this.emit('crashGameUpdate', {
      state: STATE_STARTING,
      startTime: this.startTime
    })
    // this.broadcast()


    setTimeout(() => {
      // Randomize crash
      this.state = STATE_IN_PROGRESS
      this.startTime = Date.now()
      this.endsAt = this.startTime + Math.floor(((Math.random() * 15) * 1000) + 1000)

      this.tickInterval = setInterval(() => {
        this.emit('crashTick', Date.now() - this.startTime)
      }, 150)

      setTimeout(() => {
        clearInterval(this.tickInterval)

        // The game finished
        this.state = STATE_OVER
        this.broadcast({
          elapsed: this.endsAt - this.startTime
        })

        setTimeout(() => this.start(), 2000) // Loop all over again
      }, this.endsAt - Date.now())

      this.emit('crashGameUpdate', {
        state: this.state,
        elapsed: Date.now() - this.startTime
      })

      // this.broadcast()
    }, this.startTime - Date.now())
  }

  broadcast(extra = {}) {
    this.emit('crashGameUpdate', {
      ...extra,

      state: this.state,
      startTime: this.startTime
    })
  }
}

// Make it seem as if it was a socket.io client or something
ee(FakeGame.prototype)

export default class Demo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      gameState: STATE_STARTING,
      gameStartedAt: null,
      gameElapsed: null,
      lagDetected: false
    }

    this.fakeGame = new FakeGame()
    this._lastTick = null
    this._lagTimer = null
  }

  componentDidMount() {
    this.fakeGame.on('crashGameUpdate', update => {
      console.log('crashGameUpdate', update)

      if(update.state === STATE_OVER) {
        this.setState({
          gameState: update.state,
          gameElapsed: update.elapsed
        })
      } else if(update.state === STATE_STARTING) {
        this.setState({
          gameState: update.state,
          gameStartedAt: update.startTime
        })
      } else if(update.state === STATE_IN_PROGRESS) {
        this.setState({
          gameState: update.state,
          gameStartedAt: Date.now() - update.elapsed
        })
      }
    })

    this.fakeGame.on('crashTick', elapsed => {
      if(this.state.gameState !== STATE_IN_PROGRESS) {
        return
      }

      this._lastTick = Date.now()

      if(this.state.lagDetected) {
        this.setState({
          lagDetected: false
        })
      }

      const currentLatencyStartTime = this._lastTick - elapsed

      if(this.state.gameElapsed > currentLatencyStartTime) {
        this.setState({
          gameElapsed: currentLatencyStartTime
        })
      }

      if(this._lagTimer) {
        clearTimeout(this._lagTimer)
      }

      this._lagTimer = setTimeout(() => {
        this.setState({
          lastTick: this._lastTick,
          lagDetected: true
        })
      }, 300)
    })

    this.fakeGame.start()
  }

  render() {
    const { gameState, gameStartedAt, gameElapsed, lagDetected, lastTick } = this.state

    return (
      <div className={style.demo}>
        <CrashGraph
          state={gameState}
          startTime={gameStartedAt}
          delayed={lagDetected}
          lastTick={lastTick}
          elapsed={gameElapsed} />
        lagging ? {lagDetected ? 'yes' : 'no'}
      </div>
    )
  }
}

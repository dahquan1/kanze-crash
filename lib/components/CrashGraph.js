
import React, { PureComponent } from 'react'; // eslint-disable-line
import style from './style.css'

const XTICK_LABEL_OFFSET  = 25
const XTICK_MARK_LENGTH   = 2
const YTICK_LABEL_OFFSET  = 30
const YTICK_MARK_LENGTH   = 2

export const STATE_NOT_STARTED = 'NotStarted'
export const STATE_STARTING = 'Starting'
export const STATE_IN_PROGRESS = 'InProgress'
export const STATE_OVER = 'Over'

export class CrashGraph extends PureComponent {
  constructor(props) {
    super(props)

    this._currentTime = null
    this._currentGamePayout = null
    this._delayed = false
    this._lastCrash = null

    this._rendering = true
  }

  render() {
    return (
      <div ref="container" className={style.crashGraph}>
        <canvas ref="canvas" />
      </div>
    )
  }

  _renderGraph() {
    if(!this._rendering) {
      return
    }

    this._calcGameData()
    this._calculatePlotValues()
    this._cleanChart()

    this._renderAxes()
    this._renderLine()
    this._renderGameData()
    // this._renderLoader()

    this._animRequest = window.requestAnimationFrame(::this._renderGraph)
  }

  _renderGameData() {
    const { state, startTime } = this.props
    const ctx = this._ctx

    // One percent of canvas width
    const onePercent = this._canvasWidth / 100

    ctx.fillStyle = '#000'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'

    if(state === STATE_NOT_STARTED) {
      ctx.font = this._fontSizePx(8) + ' Verdana'
      ctx.fillText('Connecting...', this._canvasWidth / 2, this._canvasHeight / 2)
    }

    if(state === STATE_IN_PROGRESS) {
      ctx.font = this._fontSizePx(14) + ' Verdana'
      ctx.fillText(parseFloat(this._currentGamePayout).toFixed(2) + 'x', this._canvasWidth / 2, this._canvasHeight / 2)
    }

    if(state === STATE_OVER && !!this.props.elapsed) {
      const crashedAt = this._calculateGamePayout(this.props.elapsed)
      ctx.font = this._fontSizePx(14) + ' Verdana'
      ctx.fillStyle = 'red'
      ctx.fillText(parseFloat(crashedAt).toFixed(2) + 'x', this._canvasWidth / 2, this._canvasHeight / 2)
    }

    if(state === STATE_STARTING) {
      const timeLeft = (this.props.startTime / 1000) - (Date.now() / 1000)

      ctx.font = this._fontSizePx(14) + ' Verdana'
      ctx.fillText(timeLeft <= 0 ? 'Starting...' : `${Math.max(0, timeLeft).toFixed(1)}s`, this._canvasWidth / 2, this._canvasHeight / 2)
    }
  }

  _renderAxes() {
    const ctx = this._ctx

    const stepValues = x => {
      console.assert(Number.isFinite(x));

      let c = .4
      let r = .1

      while (true) {
        if (x <  c) {
          return r
        }

        c *= 5
        r *= 2

        if (x <  c) {
          return r
        }

        c *= 2
        r *= 5
      }
    }

    // Calculate Y Axis
    this._YAxisPlotMaxValue = this._YAxisPlotMinValue
    this._payoutSeparation = stepValues(!this._currentGamePayout ? 1 : this._currentGamePayout)

    ctx.setLineDash([10, 6])

    ctx.lineWidth = 3
    ctx.strokeStyle = '#a9a9a9'
    ctx.font = '14px Helvetica,Arial,sans-serif'
    ctx.fillStyle = '#a9a9a9'
    ctx.textAlign = 'center'

    if(this.props.state === STATE_OVER && this._lastCrash) {
      ctx.fillStyle = 'red'
    }

    // Draw Y Axis Values
    const heightIncrement =  this._plotHeight / (this._YAxisPlotValue)

    for(let payout = this._payoutSeparation, i = 0; payout < this._YAxisPlotValue; payout+= this._payoutSeparation, i++) {
        let y = this._plotHeight - (payout * heightIncrement)
        ctx.fillText((payout + 1).toFixed(2) + 'x', 25, y)

        ctx.beginPath()
        ctx.moveTo(this._xStart, y - 1)
        ctx.lineTo(this._canvasWidth , y - 1)
        ctx.stroke()

        if(i > 100) {
          console.log('For 3 too long')
          break
        }
    }

    // Calculate X Axis
    this._milisecondsSeparation = stepValues(this._XAxisPlotValue)
    this._XAxisValuesSeparation = this._plotWidth / (this._XAxisPlotValue / this._milisecondsSeparation)

    // Draw X Axis Values
    for(let miliseconds = 0, counter = 0, i = 0; miliseconds < this._XAxisPlotValue; miliseconds += this._milisecondsSeparation, counter++, i++) {
      let seconds = miliseconds / 1000
      let textWidth = ctx.measureText(seconds).width
      let x = (counter * this._XAxisValuesSeparation) + this._xStart

      ctx.fillText(seconds + 's', x - textWidth / 2, this._plotHeight + 30)

      if(i > 100) {
        console.log('For 4 too long')
        break
      }
    }

    // Draw background Axis
    ctx.lineWidth = 3
    ctx.beginPath()
    // ctx.moveTo(this._xStart, 0)
    ctx.moveTo(this._xStart, this._canvasHeight - this._yStart)
    ctx.lineTo(this._canvasWidth, this._canvasHeight - this._yStart)
    ctx.stroke()
  }

  _renderLine() {
    const ctx = this._ctx

    ctx.strokeStyle = this._gradient
    ctx.fillStyle = this._gradient
    ctx.lineWidth = 3
    ctx.setLineDash([ 20, 5 ])
    ctx.beginPath()

    for(let t = 0, i = 0; t <= this._currentTime; t += 100, i++) {
      let payout = this._calculateGamePayout(t) - 1
      let y = this._plotHeight - (payout * this._heightIncrement)
      let x = t * this._widthIncrement

      ctx.lineTo(x + this._xStart, y)

      /* Avoid crashing the explorer if the cycle is infinite */
      if(i > 5000) {
        console.log('For 1 too long!')
        break
      }
    }

    ctx.stroke()
    ctx.closePath()
  }

  componentDidMount() {
    // Setup the chart
    const { canvas, container } = this.refs
    if(!canvas.getContext) {
      throw new Error('Cannot get canvas context')
    }

    this._ctx = canvas.getContext('2d')

    this._onResize = () => {
      this._canvasWidth = container.clientWidth
      this._canvasHeight = container.clientHeight
      this._configPlotSettings()
    }

    this._onResize()
    window.addEventListener('resize', this._onResize)
    this._animRequest = window.requestAnimationFrame(::this._renderGraph)
  }

  componentWillUnmount() {
    this._rendering = false
    window.removeEventListener('resize', this._onResize)
  }

  _calcGameData() {
    this._currentTime = this._getElapsedTimeWithLag(this.props.startTime)
    this._currentGamePayout = this._calculateGamePayout(this._currentTime)
  }

  _calculatePlotValues() {
     this._YAxisPlotMinValue = this._YAxisSizeMultiplier
     this._YAxisPlotValue = this._YAxisPlotMinValue
     this._XAxisPlotValue = this._XAxisPlotMinValue

     if(this._currentTime > this._XAxisPlotMinValue) {
       this._XAxisPlotValue = this._currentTime
     }

     // Adjust Y Plot's Axis
     if(this._currentGamePayout > this._YAxisPlotMinValue) {
       this._YAxisPlotValue = this._currentGamePayout
     }

     this._YAxisPlotValue -= 1

     // Graph values
     this._widthIncrement = this._plotWidth / this._XAxisPlotValue
     this._heightIncrement = this._plotHeight / (this._YAxisPlotValue)
     this._currentX = this._currentTime * this._widthIncrement
  }

  _configPlotSettings() {
    const { canvas, container } = this.refs

    canvas.width = this._canvasWidth
    canvas.height = this._canvasHeight

    this._plotWidth = this._canvasWidth - 50
    this._plotHeight = this._canvasHeight - 45
    this._xStart = this._canvasWidth - this._plotWidth
    this._yStart = this._canvasHeight - this._plotHeight
    this._XAxisPlotMinValue = 1000
    this._YAxisSizeMultiplier = 1.5
   }

   _getElapsedTimeWithLag() {
     const { startTime } = this.props

     if(this.props.state == STATE_IN_PROGRESS) {
       let elapsed = 0

       if(this.props.delayed) {
         elapsed = this.props.lastTick - startTime + 300
       } else {
         elapsed = this._getElapsedTime(startTime)
       }

       return elapsed
     }

     return 0
   }

   _getElapsedTime(startTime) {
     return Date.now() - this.props.startTime
   }

   _calculateGamePayout(ms) {
     const gamePayout = Math.floor(100 * this._growthFunc(ms)) / 100
     console.assert(Number.isFinite(gamePayout))
     return Math.max(gamePayout, 1)
   }

   // Function to calculate the distance in semantic values between ticks. The
   // parameter s is the minimum tick separation and the function produces a
   // prettier value.
   _tickSeparation(s) {
     if (!Number.isFinite(s)) {
       throw new Error('Is not a number: ', s)
     }

     let r = 1
     while(true) {
       if (r > s) {
         return r
       }

       r *= 2

       if (r > s) {
         return r
       }

       r *= 5
     }
   }

   // Measure the em-Height by CSS hackery as height text measurement is not
   // available on most browsers. From:
   // https://galacticmilk.com/journal/2011/01/html5-typographic-metrics/#measure
   _getEmHeight(font) {
     const sp = document.createElement('span')
     sp.style.font = font
     sp.style.display = 'inline'
     sp.textContent = 'Hello world!'

     document.body.appendChild(sp)
     const emHeight = sp.offsetHeight
     document.body.removeChild(sp)
     return emHeight
   }

   // _fontSizeNum
   _fontSizeNum = times => times * this._canvasWidth / 100

   // _fontSizePx
   _fontSizePx = times => this._fontSizeNum(times).toFixed(2) + 'px'

   // _trX
   _trX = t => this._XScale * (t - this._XTimeBeg)

   // _trY
   _trY = p => - (this._YScale * (p - this._YPayoutBeg))

   // _cleanChart
   _cleanChart = () => this._ctx.clearRect(0, 0, this._canvasWidth, this._canvasHeight)

   // _growthFunc
   _growthFunc = ms => Math.pow(Math.E, 0.00006 * ms)
}

export default CrashGraph;
